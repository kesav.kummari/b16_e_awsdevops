#### Session Video :

```
Part-1 : https://drive.google.com/file/d/1E9t7sm-ecBfAC05KmK25VLwphdzYVj02/view?usp=drive_link

Part-2 : https://drive.google.com/file/d/1QvIAieua7EzrMdvBUnKxXuUeYBP36yiW/view?usp=drive_link

```
#### Agenda :

1. Download Install & Configure prerequisites:
    - Host Machine :
        - Windows : 1. Chrome, 2. Git 3. Visual Studio Code
        - MacOs   : 1. Chrome, 2. Git 3. Visual Studio Code
        - Linux   : 1. Chrome, 2. Git 3. Visual Studio Code


2. Create Account with GitLab

3. Create Account with AWS 
    
