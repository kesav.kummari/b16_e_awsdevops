#### Session Video :

```
https://drive.google.com/file/d/1UBMvUk3WDfQtxbtsmHa2Ei2_n65lWpQw/view?usp=sharing
```

#### Agenda :

```
AWS --> Compute --> EC2 :
  - Windows
  - Linux
  
```

#### IAM

Here's an example policy (modify as needed):

```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:Describe*",
        "ec2:StartInstances",
        "ec2:StopInstances",
        "ec2:RebootInstances"
      ],
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "ec2:Region": "us-east-1"
        }
      }
    }
  ]
}

```