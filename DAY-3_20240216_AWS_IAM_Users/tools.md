#### Session Video :

```
https://drive.google.com/file/d/15JeYlhXFtGvo4Ig3obaaEwkq2Hkcji5l/view?usp=sharing

```
#### Agenda :



  IAM Users

  IAM Groups

  IAM Policies



#### IAM

Here's an example policy (modify as needed):



```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:Describe*",
        "ec2:StartInstances",
        "ec2:StopInstances",
        "ec2:RebootInstances"
      ],
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "ec2:Region": "us-east-1"
        }
      }
    }
  ]
}

```